/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/things              ->  index
 * POST    /api/things              ->  create
 * GET     /api/things/:id          ->  show
 * PUT     /api/things/:id          ->  update
 * DELETE  /api/things/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Thing = require('./thing.model');

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(function(updated) {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(function() {
          res.status(204).end();
        });
    }
  };
}

// Gets a list of Things
exports.index = function(req, res) {
  Thing.findAsync()
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Gets a single Thing from the DB
exports.show = function(req, res) {
  Thing.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Creates a new Thing in the DB
exports.create = function(req, res) {
  Thing.createAsync(req.body)
    .then(responseWithResult(res, 201))
    .catch(handleError(res));
};

// Updates an existing Thing in the DB
exports.update = function(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Thing.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
};

// Deletes a Thing from the DB
exports.destroy = function(req, res) {
  Thing.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
};


//API key
//feb92eaa7cb61996b28af87941eb23c2-us11

exports.subscribe = function(req, res){
  
  var mcapi = require('mailchimp-api');

  var mc = new mcapi.Mailchimp('feb92eaa7cb61996b28af87941eb23c2-us11');

   /*mc.campaigns.list({'status':'sent'}, function(data) {
    res.json({ title: 'Report', campaigns: data.data });
  }, function(error) {
    if (error.error) {
      req.session.error_flash = error.code + ": " + error.error;
    } else {
      req.session.error_flash = "An unknown error occurred";
    }
    res.redirect("/");
  });*/

  var email_res = req.body.email;
  var name_res = req.body.name;
  var last_res = req.body.last;
  console.log('Email:'+email_res);
    console.log('Name:'+name_res);
      console.log('Last:'+last_res);
   var mcReq = {
        id: 'dff590ca0e',
        email: { email: email_res },
        merge_vars: {
            EMAIL: email_res,
            FNAME: name_res,
            LNAME: last_res
        }
    };
    // submit subscription request to mail chimp
    mc.lists.subscribe(mcReq, function(data) {        
        res.json({ 'Respuesta': data });
    }, function(error) {
        res.json({ 'Error': error });
    });

};